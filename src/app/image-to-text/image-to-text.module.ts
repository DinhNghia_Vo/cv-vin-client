import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageToTextComponent } from './image-to-text.component';
import {NgbButtonsModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
    declarations: [ImageToTextComponent],
    exports: [
        ImageToTextComponent
    ],
    imports: [
        CommonModule,
        NgbButtonsModule
    ]
})
export class ImageToTextModule { }
