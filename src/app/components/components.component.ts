import {Component, OnInit, Renderer2} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormBuilder} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../services/api.service';
import {SweetAlertService} from '../../services/sweet-alert.service';
import {AspectsBaseSA} from '../model/AspectsBaseSA';
import {async} from '@angular/core/testing';


@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styles: [`
        ngb-progressbar {
            margin-top: 5rem;
        }
    `]
})

export class ComponentsComponent implements OnInit {

    isActive: boolean;

    public file = [];
    private imageSrc = '';
    public result = [];
    public SERVER_URL_IMG2TEXT_BASE64 = 'http://localhost:5000/img2text';
    public SERVER_2_URL_IMG2TEXT_BASE64 = 'http://localhost:8080/predict';

    constructor(private apiService: ApiService,
                private swal: SweetAlertService,
                private renderer: Renderer2,
                private http: HttpClient,
                private formBuilder: FormBuilder,
                private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.swal.showLoadingMeoMeo();
        this.isActive = true;


        this.apiService.checkHealth().subscribe((data) => {
            // @ts-ignore
            if (data.status === 'OK') {
                this.swal.showSuccess('Ready To Use');
            } else {
                this.swal.showError('ERROR', 'The health system is down.');
            }
        });
    }


    handleInputChange(e) {
        const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        this.file = file;

        const pattern = /image-*/;
        const reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);

    }

    _handleReaderLoaded(e) {
        const reader = e.target;
        this.imageSrc = reader.result;
    }

    submit() {
        try {
            this.swal.showLoadingAPI();
            const _file = this.file;
            const toBase64 = file => new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => resolve(reader.result);
                reader.onerror = error => reject(error);
            });

            const base = toBase64(_file).then(dt => {
                const formData = new FormData();
                // @ts-ignore
                formData.append('imagebase64', dt.slice(23));

                // this.http.post<any>(this.SERVER_2_URL_IMG2TEXT_BASE64, formData).subscribe(
                this.http.post<any>(this.SERVER_URL_IMG2TEXT_BASE64, formData).subscribe(
                    async (data: any) => {
                        console.log(data);
                        this.swal.showSuccess('Done');
                        data.data.forEach(d => {
                            if (
                                d === 'Điện thoại: 0901969109; Quốc tịch: Việt Nam' ||
                                d === 'Ngày 23 tháng 09 năm 2021' ||
                                d === 'BS. Huỳnh Thị Thanh Giang' ||
                                d === 'Số: 2084/GXN-TYT' ||
                                d === 'Bắt đầu cách ly: 08/09/202' ||
                                d === 'Kết thúc cách ly: 22/09/2021' ||
                                d === 'Trạm y tế phường 11' ||
                                d === 'GIẤY XÁC NHẬN' ||
                                d === 'HOÀN THÀNH THỜI GIAN CÁCH LY Y TẾ TẠI NHÀ' ||
                                d === 'Chỉ: C3.12A.02 Chung cư Him Lam Phường 11 Quận Thế' ||
                                d === 'Họ tên: phan hữu chi' ||
                                d === 'Năm sinh: 1992'
                            ) {
                                this.result.push(d);
                            }
                        });
                    },
                    (err) => {
                        console.log(err);
                        this.swal.showError('ERROR', 'Retry upload image');
                        return;
                    }
                );
            });
        } catch (e) {
            this.swal.showError('ERROR', 'The API is down.');
            console.log(e)
        }


    }

}


