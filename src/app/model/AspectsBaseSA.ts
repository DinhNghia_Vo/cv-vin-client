export class AspectsBaseSA {
    public sentimentScore: number;
    public sentimentType: string;
    public text: string;
    public topic: string;

    static fromJson(data): AspectsBaseSA {
        console.log(data);
        return new AspectsBaseSA(data.sentiment_score, data.sentiment_type, data.text, data.topic);
    }

    constructor(sentimentScore: number, sentimentType: string, text: string, topic: string) {
        this.sentimentScore = sentimentScore;
        this.sentimentType = sentimentType;
        this.text = text;
        this.topic = topic;
    }

}


