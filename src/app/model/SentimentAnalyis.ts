export class SentimentAnalyis {
    public text: string;
    public tagName: string;
    public confidence: number

    static fromJson(data): SentimentAnalyis {
        return new SentimentAnalyis(data.text, data.tag_name, data.confidence);
    }


    constructor(text: string, tagName: string, confidence: number) {
        this.text = text;
        this.tagName = tagName;
        this.confidence = confidence;
    }

}
