import {Injectable} from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class SweetAlertService {

    constructor() {
    }

    showLoadingMeoMeo() {
        Swal.fire({
            allowOutsideClick: false,
            title: 'Đợi xíu , để gọi server dậy!',
            width: 600,
            padding: '3em',
            background: '#fff url(https://sweetalert2.github.io/images/trees.png)',
            backdrop: `
    rgba(0,0,123,0.4)
    url("https://sweetalert2.github.io/images/nyan-cat.gif")
    left top
    no-repeat
  `
        })
        Swal.showLoading();
    }


    showLoadingAPI() {
        Swal.fire({
            allowOutsideClick: false,
            title: 'Đang xử lý ',
            width: 600,
            padding: '3em',
            background: '#fff url(https://sweetalert2.github.io/images/trees.png)',
            backdrop: `
    rgba(0,0,123,0.4)
    url("https://sweetalert2.github.io/images/nyan-cat.gif")
    left top
    no-repeat
  `
        })
        Swal.showLoading();
    }

    showLoadingNone() {
        Swal.fire({
            allowOutsideClick: false,
            title: 'Đợi xíu , để gọi server dậy!',
            width: 600,
            padding: '3em'
        })
        Swal.showLoading();
    }

    hideLoading() {
        Swal.close();

    }

    showError(title, text) {
        Swal.fire({
            icon: 'error',
            title: title,
            text: text,
            background: '#fff url(https://sweetalert2.github.io/images/trees.png)'
        });
    }

    showSuccess(title) {
        return Swal.fire({
            icon: 'success',
            title: title,
            showConfirmButton: false,
            timer: 1500,
            width: 600,
            padding: '3em'
        })
        Swal.hideLoading();
    }

    showSuccessMeoMeo(title) {
        return Swal.fire({
            icon: 'success',
            title: title,
            showConfirmButton: false,
            width: 600,
            padding: '3em',
            backdrop: `
    rgba(0,0,123,0.4)
    url("https://sweetalert2.github.io/images/nyan-cat.gif")
    left top
    no-repeat
  `
        })
        this.showSuccessMeoMeo(title)
    }

    showMeoMeo() {
        Swal.fire({
            title: 'Đợi xíu , để gọi server dậy!',
            width: 600,
            padding: '3em',
            background: '#fff url(https://sweetalert2.github.io/images/trees.png)',
            backdrop: `
    rgba(0,0,123,0.4)
    url("https://sweetalert2.github.io/images/nyan-cat.gif")
    left top
    no-repeat
  `
        })
    }
}
